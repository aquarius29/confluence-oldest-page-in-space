The script will scan through the confluence via REST api and get all the 
pages in all the spaces and will create a post with a table of oldest page 
per space and who owns that page. As owner is considered the person who was the 
last editor of the page. In case when the last editor is inactive (not working 
at the company any longer) then the script will search the editors history 
and will find the latest active editor. If non of the history editors are 
active then the page will be marked as orphan. 

In the table the name of the last editor will be mentioned so the last editor will 
get an email notification that he/she was mentioned on the page.
The intention is that the person will go to the page and see that he/she owns the 
oldest page in particular space. Then this old page should either be edited to make
sure the information is up to date or it should be deleted/archieved so it does not 
hang around and confuses the search engine. 


TO be done : 

 ** create a test page for test runs and define the test page in the config.ini 
 ** add csv file with history tracking to see how the changes happen over time - attach the file to the page.
 ** MAYBE - crete a list of the orphan pages per space. 