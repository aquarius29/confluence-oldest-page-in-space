#python

import requests
from requests.auth import HTTPBasicAuth
import sys
import os.path
import operator 
import xml.etree.ElementTree as ET
import json 
from datetime import datetime
import configparser

# import cProfile, pstats, io

DEBUG = False
config = configparser.ConfigParser()
config.read('config.ini')

CONFLUENCE_LOGIN = config['confluence']['CONFLUENCE_LOGIN']
CONFLUENCE_PASSWORD = config['confluence']['CONFLUENCE_PASSWORD']
API_URL = config['confluence']['API_URL']

def store_information_for_space(space_key):
	pages = get_all_pages_data_in_space(space_key)
	pages_in_space = []
	data_for_posting = []
	counter = 0
	for page in pages:
		if page['status'] == 'current':
			counter += 1
			# page_history = page['history']
			pageid = page['id']
			# print(str(len(pages) - counter) + " to go for space " + space_key)
			pgdata = PageData(pageid)
			pgdata.setPageData(page)
			pgdata.setHistoryData(page['history'])
			pgdata.setSpace(space_key)
			pages_in_space.append(pgdata)
	sorted_pages_in_space = sorted(pages_in_space,key=operator.attrgetter('lastmodified'))	
	if len(pages) > 0:
		return sorted_pages_in_space[0], len(pages)
	else:
		return None, None
	# for pgdata in sorted_pages_in_space:
	# 	write_to_file(pgdata)

def write_to_file(pgdata):
	###  the idea is to write file with history data, chagnes every day
	f = open("post_msg.txt", "wb")	
	# f.write(pgdata.space + ";" + pgdata.pageid + ";" + pgdata.pagetitle + ";" + pgdata.pageurl + ";" + pgdata.createdby.displayname + ";" + pgdata.createdby.useremail + ";" + pgdata.created + ";" + pgdata.lastmodifiedby.displayname +";"  + pgdata.lastmodifiedby.useremail + ";" + pgdata.lastmodified + "\n")
	f.write(pgdata)
	f.close()

def get_space_ids():
	get_all_spaces = False
	# url = '/rest/api/space/?limit=1&start=28'
	url = '/rest/api/space/?limit=500'
	spaces = []
	while not get_all_spaces:
		json_data = make_api_call(url)
		spaces = spaces + json_data['results']
		if "next" not in json_data['_links']:
			get_all_spaces = True
	return spaces


def get_all_pages_data_in_space(space_key):
	# should get the page id, who created, when created, when last modified, who lastmodified
	get_all_pages = False
	# url = "/rest/api/content?spaceKey=" + space_key + "&limit=500"
	url = "/rest/api/space/"  + space_key + "/content/page/?limit=200&expand=history.lastUpdated"
	pages = []
	total_pages = 0
	while not get_all_pages:
		json_data = make_api_call(url)
		pages = pages + json_data['results']
		total_pages += json_data['size']
		print("just got " + str(total_pages) + " from space " + space_key)
		if "next" not in json_data['_links']:
			get_all_pages = True
		else:
			url = json_data['_links']['next']
	return pages

def make_api_call(url_suffix, full_link=False):
	if DEBUG:
		print(" --- >>> making api call < ---")
		print("url: " + url_suffix)
	if full_link:
		url = url_suffix
	else:
		url = API_URL + url_suffix
	r = requests.get(url, auth=HTTPBasicAuth(CONFLUENCE_LOGIN, CONFLUENCE_PASSWORD))
	return r.json()



class PageData:
	def __init__(self,pageid):
		self.pageid = pageid
		self.space = None
		self.createdby = None
		self.created = None
		self.lastmodifiedby = None
		self.lastmodified = None
		self.pageurl = None
		self.pagetitle = None
		self.currentversion = None
		# self.setPageDataFromID()

	def setCreatedBy(self, createdBy):
		#createdBy User()
		self.createdby = createdBy
	
	def setCreated(self, createdDate):
		self.created = createdDate

	def setLastModified(self, lastmodified):
		self.lastmodified = lastmodified

	def setLastModifiedBy(self, lastmodifiedby):
		self.lastmodifiedby = lastmodifiedby

	def setPageUrl(self, url):
		self.pageurl = url

	def setPageTitle(self, title):
		self.pagetitle = title

	def setPageCurrentVerion(self,number):
		self.currentversion = number

	def setPageDataFromID(self):
		page_json = make_api_call("/rest/api/content/" + str(self.pageid) + "?expand=history.lastUpdated,space")
		self.setPageData(page_json)
		self.setHistoryData(page_json['history'])
		self.setSpace(page_json['space']['key'])

	def setSpace(self, space_key):
		self.space = space_key

	def setHistoryData(self, history_json):
		#will set the 
		# setLastModified
		# setLastModifiedBy
		# setPageCurrentVerion
		lastUpd = history_json['lastUpdated']
		lastupdate = lastUpd['when']
		try:
			lastupdatedBy = User(lastUpd['by']['username'], lastUpd['by']['userKey'])
		except KeyError:
			lastupdatedBy = User("Anonymous", "Anonymous")

		lastupdatedBy.setDisplayName(lastUpd['by']['displayName'])
		# lastupdatedBy.setUserEmail()
		self.setLastModified(lastupdate) 
		self.setLastModifiedBy(lastupdatedBy)
		self.setPageCurrentVerion(lastUpd['number'])

	def setPageData(self, page):
		#  setCreated
		#  setCreatedBy
		#  setPageUrl
		#  setPageTitle
		page_history = page['history']
		pageid = page['id']

		if DEBUG:
			print(page)
		try:
			createdby = User(page_history['createdBy']['username'],page_history['createdBy']['userKey'])
		except KeyError:
			createdby = User("Anonymous", "Anonymous")
		if DEBUG:
			print('page id ' + str(pageid) + " page url:" + str(API_URL + page['_links']['webui']))
		# createdby.setUserEmail()
		createdby.setDisplayName(page_history['createdBy']['displayName'])
		self.setCreated(page_history['createdDate'])
		self.setCreatedBy(createdby)
		self.setPageUrl(API_URL + page['_links']['webui'])
		self.setPageTitle(page['title'])

	def findFirstActiveContributor(self):
		page_version_url = "/rest/experimental/content/" + self.pageid + "/version?limit=200"
		version_json = make_api_call(page_version_url)
		for version in version_json['results']:
			if "inactive" not in version['by']['displayName'].lower():
				user = User(version['by']['username'],version['by']['userKey'])
				user.setDisplayName(version['by']['displayName'])
				return user
		return None

class User:
	def __init__(self, username, userkey):
		self.username = username
		self.userkey = userkey
		self.useremail = None
		self.displayname = None

	def getUsername(self):
		return self.username

	def getUserkey(self):
		return self.userkey

	def getDisplayName(self):
		if self.displayname:
			return self.displayname
		else:
			self.username
	
	def setDisplayName(self,displayname):
		self.displayname = displayname

	def setUserEmail(self):
		url = API_URL + "/rest/prototype/1/user/non-system/" + self.username
		r = requests.get(url, auth=HTTPBasicAuth(CONFLUENCE_LOGIN, CONFLUENCE_PASSWORD))
		if DEBUG:
			print(r.text)
		try:
			tree = ET.fromstring(r.text)
			emails = tree.findall('displayableEmail')
			email_long = emails[0].text
			email_long = email_long.replace(' dot ', '.')
			email = email_long.replace(' at ', '@')
			self.useremail = email
		except:
			self.useremail = "NONE"

def lets_do_it():
	spaces = get_space_ids()
	post_data = {}
	for space in spaces:
		if space['type'] == "personal":
			continue
		statistics = {}
		space_key = space['key']
		print("---------------------------------")
		print("Starting with space " + space_key)
		oldest_page, total_pages = store_information_for_space(space_key)
		if not oldest_page:
			continue
		# print(oldest_page, str(total_pages))
		statistics['oldest_page'] = oldest_page
		statistics['total_pages'] = total_pages
		post_data[space_key] = statistics
	if DEBUG:
		print(" collected this data : ")
		print(post_data)
	post_list_of_spaces_stats(post_data)

def post_list_of_spaces_stats(post_data):
	notification_days = int(config['confluence']['old_page_cutout'])
	date_today = datetime.now()
	table_rows = ""
	page_id = config['confluence']['page_id']
	post_page = PageData(page_id)
	post_page.setPageDataFromID()
	for space_key in post_data: 
		print( " analyzing space " + space_key)
		# pages = get_all_pages_data_in_space(space_key)
		total_pages_in_space = post_data[space_key]['total_pages']
		oldest_page = post_data[space_key]['oldest_page']
		page_url = oldest_page.pageurl
		page_title = "<a href='" + oldest_page.pageurl + "'>" + oldest_page.pagetitle.replace("&", "&amp;") + "</a>"
		last_modified = oldest_page.lastmodified[0:16]
		if (date_today - datetime.strptime(last_modified,'%Y-%m-%dT%H:%M')).days > notification_days :
			last_modifier = "<ac:emoticon ac:name='warning'/> " + oldest_page.lastmodifiedby.displayname
			###### THIS NEXT LINE WILL REPLACE THE MODIFIER WITH NOTIFIABLE LINK
		# last_modifier = "<ac:link><ri:user ri:userkey=" + oldest_page.lastmodifiedby.userkey + "/></ac:link>" 
		else:
			last_modifier = oldest_page.lastmodifiedby.displayname
		if "inactive" in  last_modifier.lower():
			last_active_modifier = oldest_page.findFirstActiveContributor()
			if not last_active_modifier : 
				last_active_modifier_str  = "Orphan Page"
			else:
				last_active_modifier_str = last_active_modifier.displayname
			last_modifier =  last_modifier + " <strong>" + last_active_modifier_str + "</strong>"
		row_template = '''<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>''' % (space_key, str(total_pages_in_space),page_title, last_modifier, last_modified)
		table_rows += row_template
	table_template = '''<table class='wrapped'><tbody><tr><th>space</th><th>pages</th><th>oldest page(OP)</th><th>OP last modifier</th><th>OP last modified date</th></tr>%s</tbody></table>''' % (table_rows)
	pretable_text = '''This page has the list of spaces on our confluence. <br/>You can also see the oldest page on that space (oldest meaning nobody modified that page), the last modifier of the page (person who modified the oldest page the last) and the last modification date. If your name is listed here then you should go to that oldest page and see if it is still relevant and maybe update it or remove it from confluence if the page it completely outdated. <br/> <ac:emoticon ac:name='warning'/> means that the page is more than %i days old. <br/><strong>Orphan page</strong> means that all contributors to the page are inacvite.''' % (notification_days)
	print(" -------------------------------- ")
	print('data is collected will post now! ')
	post_url = API_URL + "/rest/api/content/" + post_page.pageid
	content_json ='{"id":' +str(post_page.pageid) + ', "type":"page", "title":"Old pages on Confluence stats", "space":{"key":"~' + CONFLUENCE_LOGIN + '"}, "body":{"storage":{"value":"' + pretable_text + table_template + '", "representation":"storage"}}, "version":{"number":' + str(post_page.currentversion + 1) + '}}'
	# print(content_json.encode('utf-8'))
	header = json.loads('{"X-Atlassian-Token": "no-check", "Content-Type": "application/json"}')
	write_to_file(content_json.encode('utf-8'))
	r = requests.put(post_url, auth=HTTPBasicAuth(CONFLUENCE_LOGIN, CONFLUENCE_PASSWORD), data=content_json.encode('utf-8'), headers=header)
	if r.status_code == 200:
		print(" Success ! the new data should be on the page.")
	if DEBUG:
		print(r.text)

lets_do_it()

